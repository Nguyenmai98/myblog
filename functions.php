<?php
function theme_settup(){
	register_nav_menu('topmenu',__( 'Menu chính' ));
}
add_action('init', 'theme_settup');

function theme_settup2(){
	register_nav_menu('sidebar',__( 'Menu phụ' ));
}
add_action('init', 'theme_settup2');

function add_additional_class_on_li($classes, $item, $args) {
    if($args->add_li_class) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);

add_action('wp_ajax_Post_filters', 'Post_filters');
	add_action('wp_ajax_nopriv_Post_filters', 'Post_filters');
	function Post_filters() {
	    if(isset($_POST['data'])){
		    $data = $_POST['data']; // nhận dữ liệu từ client
		    echo '<ul>';
		    $getposts = new WP_query(); $getposts->query('post_status=publish&showposts=10&s='.$data);
		    global $wp_query; $wp_query->in_the_loop = true;
		    while ($getposts->have_posts()) : $getposts->the_post();
		        echo '<li><a target="_blank" href="'.get_the_permalink().'">'.get_the_title().'</a></li>'; 
		    endwhile; wp_reset_postdata();
		    echo '</ul>';
		    die(); 
	    }
	}

