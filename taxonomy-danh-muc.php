<?php get_header(); ?>
	<div class="container">
		
		<div class="col-sm-8 col-md-9 main-page">
		    <h2 class="title_1">SẢN PHẨM</h2>
				<ul class="products">
				 <?php if (have_posts()) : ?>
                	<?php while (have_posts()) : the_post(); ?>
                    <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
					<li>
		             <a title="Xe nâng tay cao HS-A" href="<?php the_permalink(); ?>" class="img_product">
		                <div class="tem_img">
		                   <img alt="<?php the_title(); ?>" title="<?php the_title(); ?>" src="<?php echo $featured_img_url ?>">
		                </div>
		                <h3 class="tit"><?php the_title(); ?></h3>
		                <div class="chi-tiet">
		             <a href="<?php the_permalink(); ?>" class="view">Chi tiết</a> </div>
		             </a>                               
		          </li>
				<?php endwhile; else : ?>
                <p>Không có</p>
                <?php endif; ?>                     
		          
			    </ul>
			</div>
			<?php get_sidebar(); ?>
	</div>
	<?php get_footer(); ?>
	<style type="text/css">
		.container {
		    box-shadow: 0 5px 40px 0 rgba(0,0,0,.11)!important;
		}
	</style>