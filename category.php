<?php get_header(); ?>
<div class="container">
   
    <div class="col-sm-8 col-md-9">
		<div class="main">
            <h2 class="title_1">DỊCH VỤ</h2>
            <!-- Get post mặt định -->
                <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
                    <div class="solution_item">
                     <img width="111" height="100" src="<?php echo $featured_img_url ?>" alt="<?php the_title() ;?>">
                     <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                     <p>
                     <?php the_excerpt();?>
                     </p>
                     <div class="detail-tt"><a href="<?php the_permalink(); ?>">Xem thêm</a></div>
                  </div>   
                <?php endwhile; else : ?>
                <p>Không có</p>
                <?php endif; ?>
                <!-- Get post mặt định -->
         	    
        </div>
    </div>
    <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
<style type="text/css">
    .container {
            box-shadow: 0 5px 40px 0 rgba(0,0,0,.11)!important;
        }
    .solution_item {
        width: 100%;
        position: relative;
        box-shadow: rgb(204, 204, 204) 0px 0px 4px;
        overflow: hidden;
        background: rgb(255, 255, 255);
        border-radius: 10px;
        padding: 10px 0px 10px;
        margin: 0px 0px 20px;
    }
    .solution_item img {
        float: left;
        margin: 0px 20px 0px 10px;
        position: absolute; 
        top:50%; 
        transform: translateY(-50%);
    }
    @media(max-width: 467px){
        .solution_item {
            padding: 10px;
            border: 1px solid #fbfbfb;
        }
        .solution_item img {
            float: none;
            margin: 0 20px 15px 10px;
            margin-left: auto;
            margin-right: auto;
            display: block;
        }        
    }
    a img {
        border-width: initial;
        border-style: none;
        border-color: initial;
        border-image: initial;
    }
    .solution_item h3 a {
        color: rgb(0, 137, 76);
    }
    .solution_item h3, .solution_item p{
        margin-left: 130px;
        padding: 0px 10px 0 10px;
    }
    .solution_item p {
        font-size: 14px;
        font-weight: bold;
    }
    .detail-tt {
        float: right;
        padding: 10px;
    }
    .detail-tt a {
        color: rgb(255, 255, 255);
        background: rgb(192, 157, 55);
        padding: 5px 10px;
        border-radius: 10px;
    }
</style>
