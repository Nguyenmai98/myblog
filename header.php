<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Google font -->
		<link href="https://fonts.googleapis.com/css?family=Lato:700%7CMontserrat:400,600" rel="stylesheet">
		<!-- Bootstrap -->
		<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>
		<!-- Font Awesome Icon -->
		<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/style.css">
		<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/css/all.css">
  		<script src="<?php bloginfo('template_directory') ?>/js/bootstrap.min.js"></script>
  		<script src="<?php bloginfo('template_directory') ?>/js/jquery.elevatezoom.js"></script>
  		<script src="<?php bloginfo('template_directory') ?>/js/jquery.min.js"></script>
  		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  		<?php wp_head(); ?>
    </head>
	<body>
		<div class="container">
			<div class="row" style="background: #ECECEC;">
				<div class="col-sm-6">
					<div class="contactinfo">
						<ul class="nav-pills">
							<li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="social-icons pull-right">
						<ul class="navbar-nav">
							<li><form action="<?php bloginfo('url'); ?>/" method="GET" role="form" id="demo-2"><input type="search" autocomplete="off" class="search-ajax" name="s" placeholder="Tìm kiếm">	</form>
								<div id="load-data" style="display: block;"></div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	<div class="container">
		<div class="row">
		<header id="header">

		  <div id="myCarousel" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		  	<?php $i = 0; ?>
		  	<?php $getposts = new WP_query(); $getposts->query('post_status=publish&showposts=11&post_type=slider'); ?>
			<?php global $wp_query; $wp_query->in_the_loop = true; ?>
			<?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
			<?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
				<li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" 
				class="<?php if($i==0) {echo "active"; }?>"></li>
				<?php $i++; ?>
			<?php endwhile; wp_reset_postdata(); ?>
		    
		  </ol>
		  <!-- Get post News Query -->
		  <div class="carousel-inner">
			<?php $getposts = new WP_query(); $getposts->query('post_status=publish&showposts=1&post_type=slider'); ?>
			<?php global $wp_query; $wp_query->in_the_loop = true; ?>
			<?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
			<?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
				<div class="item active">
			      <img src="<?php echo $featured_img_url; ?>" alt="xe nâng">
			    </div>
			<?php endwhile; wp_reset_postdata(); ?>
			<!-- Get post News Query -->
			<?php $getposts = new WP_query(); $getposts->query('post_status=publish&showposts=10&post_type=slider&offset=1'); ?>
			<?php global $wp_query; $wp_query->in_the_loop = true; ?>
			<?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
			<?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
				<div class="item">
			      <img src="<?php echo $featured_img_url; ?>" alt="xe nâng">
			    </div>
			<?php endwhile; wp_reset_postdata(); ?>
			</div>
		  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#myCarousel" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right"></span>
		    <span class="sr-only">Next</span>
		  </a>
		  <div class="info">
		        <h2>ROTO Việt Nam</h2>
		        <p>Chuyên phân phối và sữa chữa xe nâng, thiết bị nâng hạ</p>
		    </div>
		</div>
		<!-- navbar -->
		<div class="nav">
			    <?php $args = array(
				    'theme_location' => 'topmenu', 
				     'container' => 'false', 
				     'menu_class' => 'nav',
				     'add_li_class'  => 'nav-item'
				    );
				wp_nav_menu($args);
			    ?>
		</div>
</header>
</div>
</div>
<style type="text/css">
	#load-data{
		position: absolute;
		z-index: 1;
	    background: #fff;
	    border-radius: 4px;
	}
	#load-data li{
		margin-left: 4px;
		padding-bottom: 3px;
	}
	#load-data li a{
		color: #3B5992;
    	font-size: 15px;
    	
	}
	.nav ul {
	  list-style-type: none;
	  margin: 0;
	  padding: 0;
	  overflow: hidden;
	}

	.nav li {
	  float: left;
	  margin: 5px;
	}
	.nav li a:hover{
		background-color: white;
		color: grey;
	}
	@media(max-width: 750px){
		.nav li a{
			width: 50px;
		}
		.nav li a:hover{
			background-color: none;
			color: none;
		}
		
	}
	@media(max-width: 600px){
		.info{
			width: 165px;
			height: 70px;
		}
		.info h2{
			font-size: 12px;
		}
		.info p{
			font-size: 10px;
		}
	}
	.navbar-nav{
		position: static;
	}
	@media(max-width: 767px){
		.social-icons{
			margin-top: -15px;
			margin-bottom: -6px;
		}
	}
	@media(max-width: 750px){
		.social-icons{
			margin-top: -35px;
			margin-bottom: -6px;
		}
	}
	.nav li a {
	  display: block;
	  padding: 8px;
	  color: white;
	}
	.nav-item{
      margin-top: 5px;
    }
    .nav{
        display: flex;
        justify-content: center;
        background-color: grey;
    }
    .carousel{
        position: relative;
        margin: 0 auto;
        display: flex; /* Canh giữa bằng flex */
        justify-content: center;
        align-items: center;
    }
    .info{
        position: absolute;
        text-align: center;
        color: white;
        background-color: #1d1d1d;
        opacity: 0.7;
    }
    input {
      outline: none;
    }
    input[type=search] {
        -webkit-box-sizing: content-box;
        font-family: inherit;
        font-size: 100%;
    }
    input::-webkit-search-decoration,
    input::-webkit-search-cancel-button {
        display: none; 
    }


    input[type=search] {
        background: #ededed url(https://static.tumblr.com/ftv85bp/MIXmud4tx/search-icon.png) no-repeat 6px center;
        border: solid 1px #ccc;
        padding: 5px 6px 5px 32px;
        width: 50px;
        
        -webkit-border-radius: 10em;
        -moz-border-radius: 10em;
        border-radius: 10em;
        
        -webkit-transition: all .5s;
        -moz-transition: all .5s;
        transition: all .5s;
    }
    input[type=search]:focus {
        width: 150px;
        background-color: #fff;
        border-color: #66CC75;
        
        -webkit-box-shadow: 0 0 5px rgba(109,207,246,.5);
        -moz-box-shadow: 0 0 5px rgba(109,207,246,.5);
        box-shadow: 0 0 5px rgba(109,207,246,.5);
    }


    input:-moz-placeholder {
        color: #999;
    }
    input::-webkit-input-placeholder {
        color: #999;
    }

    /* Demo 2 */
    #demo-2 input[type=search] {
        width: 15px;
        padding-left: 10px;
        color: transparent;
        cursor: pointer;
        margin:5px;
    }
    #demo-2 input[type=search]:hover {
        background-color: #fff;
    }
    #demo-2 input[type=search]:focus {
        width: 130px;
        padding-left: 32px;
        color: #000;
        background-color: #fff;
        cursor: auto;
    }
    #demo-2 input:-moz-placeholder {
        color: transparent;
    }
    #demo-2 input::-webkit-input-placeholder {
        color: transparent;
    }
</style>