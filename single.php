<?php get_header(); ?>
<div class="container">
   
    <div class="col-sm-8 col-md-9 main-page">
    	<?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
        	<h1 class="title_1 title-only"><?php the_title(); ?></h1>
        	<p><?php the_content(); ?></p>
        	<hr>
        	<div class="other_news">
        	<?php
			    $categories = get_the_category($post->ID);
			    if ($categories) 
			    {
			        $category_ids = array();
			        foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
			 
			        $args=array(
			        'category__in' => $category_ids,
			        'post__not_in' => array($post->ID),
			        'showposts'=>5, 
			        'ignore_sticky_posts'=>1
			        );
			        $my_query = new wp_query($args);
			        if( $my_query->have_posts() ) 
			        {
			            echo '<h3 class="title">Dịch vụ liên quan</h3><ul class="list_other_news">';
			            while ($my_query->have_posts())
			            {
			                $my_query->the_post();
			                ?>
						       <li><a href="<?php the_permalink(); ?>">- <?php the_title(); ?></a></li>
			                <?php
			            }
			            echo '</ul>';
			        }
			    }
			?>

	     <?php endwhile; else : ?>
	    <p>Không có</p>
	    <?php endif; ?> 
	</div>
    </div>
    <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
<style type="text/css">
	.other_news .title {
	    color: #000000;
	    font-family: arial;
	    font-size: 12px;
	    line-height: 20px;
	    padding: 17px 0 18px;
	    text-transform: uppercase;
	}
	.other_news .list_other_news li {
	    background: url("<?php bloginfo('template_directory') ?>/img/icon_star.png ?>") no-repeat scroll left 2px rgba(0, 0, 0, 0);
	    padding-left: 23px;
	    list-style-type: none;
	}
	.other_news .list_other_news li a {
	    color: #492758;
	    font-family: arial;
	    font-size: 12px;
	    line-height: 18px;
	}
	.container {
	    box-shadow: 0 5px 40px 0 rgba(0,0,0,.11)!important;
	}
</style>