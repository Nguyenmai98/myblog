<?php get_header(); ?>
<div class="container">
    
    <div class="col-sm-8 col-md-9">
    	<div class="main main_product">
    		<?php if (have_posts()) : ?>
        	<?php while (have_posts()) : the_post(); ?>
            <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
           <h1 class="title_1 title-only"><?php the_title(); ?></h1>
           <div class="breadcrumb-row">
              <div class="left">
                 <h3 class="breadcrumb" itemscope="breadcrumb">
                    <li><a class="home" href="<?php bloginfo('url'); ?>"> Trang chủ</a></li><li><a href="<?php bloginfo('url'); ?>/san-pham">Sản phẩm</a></li><li class="active"><span><?php the_title(); ?></span></li>               
                 </h3>
              </div>
           </div>
           <div class="entry-content">
              <div id="detail_pro">
                 <div class="showimages_pro">
                    
                       <div class="zoom-n">
                             <div class="image_product">                   
                      			<img id="zoom_mw" src="<?php echo $featured_img_url ?>" data-zoom-image="<?php echo $featured_img_url ?>"/>
                              </div>
                              <div class="clearfix"></div>
                           
                       <div id="gal1">
                          <ul>
                       		 <li>
                                <img id="zoom_01" src="<?php echo $featured_img_url ?>" />
                                
                             </li>              
                          </ul>
                          <div class="clearfix"></div>
                       </div>
                 </div>
                   <script src='<?php bloginfo('template_directory') ?>/js/jquery.elevatezoom.js'></script>
                   <div class="clearfix"></div>
                
             </div>
             <div class="price_pro">
                <h4>THÔNG SỐ KỸ THUẬT CƠ BẢN:</h4>
                <ul>
                   <p><strong>Tải trọng nâng (kg):</strong><?php the_field('tai_trong_nang'); ?></p>
		          <p><strong>Chiều cao nâng thấp nhất (mm):</strong> <?php the_field('chieu_cao_nang_thap_nhat'); ?></p>

		          <p><strong>Chiều cao nâng cao nhất (mm):</strong> <?php the_field('chieu_cao_nang_cao_nhat'); ?></p>

		          <p><strong>Chiều rộng càng nâng (mm):</strong><?php the_field('chieu_rong_cang_nang'); ?></p>

		          <p><strong>Chiều dài càng nâng (mm):</strong> <?php the_field('chieu_dai_cang_nang'); ?></p>

		          <p><strong>Chiều rộng  xe (mm):</strong> <?php the_field('chieu_rong_xe'); ?></p>

		          <p><strong>Chiều dài xe (mm): </strong><?php the_field('chieu_dai_xe'); ?></p>

		          <p><strong>Chiều cao xe (mm)</strong>: <?php the_field('chieu_cao_xe'); ?></p>

		          <p><strong>Kích thước bánh nhỏ (mm):</strong> <?php the_field('kich_thuoc_banh_nho'); ?></p>

		          <p><strong>Kích thước bánh lớn (mm):</strong> <?php the_field('kich_thuoc_banh_lon'); ?></p>

		          <p><strong>Tải trọng xe (kg):</strong> <?php the_field('tai_trong_xe'); ?></p>
		                                       
		             <li class="price-nn"><b>Giá:</b><strong> Liên hệ</strong></li>
		            </ul>
		                <div class="clearfix"></div>
		             </div>
		          </div>
		          <div class="title_text">
		             <h3>Giới thiệu sản phẩm </h3>
		          </div>
		          <div class="product_intro">
		             <div class="content_text">
		                <div class="dichvu" style="min-height:100px;">
		                  <p><span style="color: rgb(37, 37, 37); font-family: Arial, Tahoma, Helvetica, sans-serif; font-size: 16.8px;"><?php the_content(); ?></span></p>
		                </div>
		             </div>
		          </div>
		          <div class="all-products-nn">
		             <div class="title_text">
		                <h3> Sản phẩm liên quan</h3>
		             </div>
		             <div class="product_intro_nn">
		                <div class="content_text">
                   <ul class="products">                  
                   	<?php
                   	$terms = get_the_terms(get_the_ID(), 'danh-muc');
                   	$current_term = $terms[0]->slug;
					    {
					    	if($current_term){
						        $args=array(
						        'danh-muc' => $current_term,
						        'post__not_in' => array(get_the_id()),
						        'showposts'=>6, 
						        'ignore_sticky_posts'=>1,
						        'post_type' => 'san-pham'
						        );
						        $my_query = new wp_query($args);
						        if( $my_query->have_posts() ) 
						        {
						            while ($my_query->have_posts())
						            {
						                $my_query->the_post(); ?>
						                <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
						                
									       <li>
						                     <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>" class="img_product">
						                        <div class="tem_img">
						                           <img alt="<?php the_title(); ?>" title="<?php the_title(); ?>" src="<?php echo $featured_img_url ?>">
						                        </div>
						                        <h3 class="tit"><?php the_title(); ?></h3>
						                        <div class="chi-tiet">
						                     <a href="<?php the_permalink(); ?>" class="view">Chi tiết</a> </div>
						                     </a>                               
						              		</li>   
						                <?php
						            }
						        }
						    }
						}
					?>
		                   

                </ul>
            </div>
         </div>
      </div>
   </div>
   <?php endwhile; else : ?>
    <p>Không có</p>
    <?php endif; ?> 
</div>  
</div>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
<style type="text/css">
	.breadcrumb {
	    padding: 8px 15px;
	    margin-bottom: 0;
	    list-style: none;
	    background-color: transparent;
	    border-radius: 4px;
	    font-size: 15px;
	}
	.breadcrumb>li {
	    display: inline-block;
	}
	.breadcrumb>.active {
	    color: #777;
	}
	#detail_pro {
	    display: inline-block;
	    position: relative;
	    width: 100%;
	}
	#detail_pro .showimages_pro {
	    float: left;
	    margin-left: 0px;
	    padding: 10px;
	    width: 50%;
	}
	.container {
		    box-shadow: 0 5px 40px 0 rgba(0,0,0,.11)!important;
		}
	div#gal1 ul li {
	    display: inline-block;
	    padding: 10px 2px;
	}
	.title_text {
	    width: 200px;
	    text-align: center;
	}
	.title_text h3 {
	    font-size: 19px;
	    background-color: blue;
	    border-radius: 5px;
	    padding: 7px;
	}
	.price_pro{
		float: left;
	}
	.product_intro {
	    background-color: #fff;
	    border: 1px solid #ccc;
	    border-radius: 5px;
	    margin: 0px auto 0;
	    padding: 0px;
	    position: relative;
	    width: 100%;
	}
	.product_intro .content_text {
	    display: inline-block;
	    font-size: 15px;
	    overflow: hidden;
	    padding: 10px 10px 20px;
	}
	.all-products-nn {
	    margin-top: 20px;
	}
	#zoom_01{
		width: 100px;
		height: 100px;
	}
</style>