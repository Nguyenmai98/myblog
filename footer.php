<footer>
	<div class="container">
	<div class="row footer">
		<div class="col-md-6 left">
			<h3><strong>ROTO Việt Nam</strong></h3>
			<p>Địa chỉ: 16/5. Tổ 10, Khu phố 2, P.Thạnh Lộc, Q.12, TP.HCM</p>
			<p>Điện thoại : 028. 37120867 - Fax: 028. 37120868</p>
			<p>Email: thaiha.xenangdatviet@gmail.com</p>
		</div>
		<div class="col-md-6 right">
			<h3><strong>ROTO Việt Nam</strong></h3>
			<p>Chi Nhánh: 176/1 Khu Phố Chiêu Liêu ,phường Tân Đông Hiệp,thị xã Dĩ An Bình Dương</p>
			<p>Điện Thoại : 0274 3775920 Fax :0274 3775921</p>
			<p>Hotline:0937 659 030 Email :Thaiha.xenangdatviet@gmail.com</p>
		</div>
	</div>
</div>
</footer>
<script>
    var timeout = null; // khai báo biến timeout
    $(".search-ajax").keyup(function(){ // bắt sự kiện khi gõ từ khóa tìm kiếm
        clearTimeout(timeout); // clear time out
        timeout = setTimeout(function (){
           call_ajax(); // gọi hàm ajax
        }, 500);
    });
    function call_ajax() { // khởi tạo hàm ajax
        var data = $('.search-ajax').val(); // get dữ liệu khi đang nhập từ khóa vào ô
        if(data == ""){
			$('#load-data').html("");
		} else {
        $.ajax({
            type: 'POST',
            async: true,
            url: '<?php echo admin_url('admin-ajax.php');?>',
            data: {
                'action' : 'Post_filters', 
                'data': data
            },
            success: function (data) {
                $('#load-data').html(data); // show dữ liệu khi trả về
            }
        });
    }
}
</script>
</body>
</html>
<style type="text/css">
	.footer p, h3{
		color: white;
	}
	.footer{
		background-color: grey;
		margin-bottom: 10px;
	}
	.right p, .right h3{
		text-align: right;
	}
</style>